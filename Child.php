<?php


class Child extends ParentBImpl implements ParentA
{

    /**
     * @var ParentA
     */
    private $parentA;

    function __construct(ParentA $parentA)
    {
        $this->parentA = $parentA;
    }

    public function doA()
    {
        $this->parentA->doA();
    }
}